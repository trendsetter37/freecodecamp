function rot13(str) {
  var ROTATE = 13;
  var STARTCHAR = 65;
  var ENDCHAR = 90;
  var ALPHABET_LENGTH = 26;
  strArr = str.split('');
  strArr = strArr.map(function (letter, idx, arr) {
    var char = str.charCodeAt(idx);
    var newChar =  (char % ALPHABET_LENGTH) + STARTCHAR;
    if (char >= STARTCHAR && char <= ENDCHAR) {
      return String.fromCharCode(newChar);
    } else {
      return letter;
    }
  });
  return strArr.join('');
}

console.log(rot13("SERR PBQR PNZC"))
