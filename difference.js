var diff = function (arr1, arr2) {
  var newArr = [];

  newArr = arr1.filter(function (val, index, arr) {
    return arr2.indexOf(val) === -1;
  });
  
  return newArr;
}

module.exports = diff;
